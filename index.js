var categories = {
    Dates: "Dates",
    TV: "Favorite Television Shows"
}

var questionsContent = {
    Dates: [
        {
            question: "What is todays year",
            name: "todaysYear",
            answers: ["Today is 1901", "Today is 1970", "2001", "2022"],
            correctAnswer: 3
        },
        {
            question: "What is the Month",
            name: "todaysMonth",
            answers: ["April", "May", "September", "December"],
            correctAnswer: 2
        },
    ],
    TV: [
        {
            question: "Favorite Documentary ",
            name: "documentary",
            answers: ["Aliens", "9 Wonders", "Reptiles", "Birds"],
            correctAnswer: 0
        },
        {
            question: "Favorite Cartoons",
            name: "cartoons",
            answers: ["Hey Arnold", "The Boondocks", "Family Guy", "South Park"],
            correctAnswer: 0
        },
    ]
}

function getOptionsHTML(options, questionName){
    var optionsHTML = '';
    for (const i in options) {
        optionId = questionName + i;
        optionsHTML += `
            <div>
                <input type="radio" id="${optionId}" name="${questionName}" value="${optionId}">
                <label for="${optionId}">${options[i]}</label>
            </div>
            `;
        }
    return optionsHTML;
      
}

function getQuestionHTML(question){
    var questionHTML = '';
    questionHTML += '<fieldset><legend>' + question.question + '</legend>' +
        getOptionsHTML(question.answers, question.name) + '</fieldset>';
    return questionHTML;
}


function getCategoryHTML(category){
    var categoryHTML = '';
    for (i =0; i < category.length; i++){
        categoryHTML += getQuestionHTML(category[i]);
    }
    return categoryHTML;
}

function getAllQuestonsHTML(){
    var allQuestionsHTML = `
        <form id="quizForm" onsubmit="return false">
    `;

    for (const property in questionsContent) {
        allQuestionsHTML += '<h5>' + categories[property] +'</h5>';
        allQuestionsHTML += getCategoryHTML(questionsContent[property]);
    }
    allQuestionsHTML += `
            <button id="submit" type="submit" onclick="generateResults()">See My Results</Button>
        </form>
    `;

    return allQuestionsHTML;
}

var quizDiv = document.getElementById('quiz');
quizDiv.innerHTML = getAllQuestonsHTML();

//===================================================

function getQuestionResultHTML(question){
    const form = document.getElementById('quizForm');
    const name = form.elements[question.name];
    if (name.value == question.name + question.correctAnswer) {
        return "correct <br />";
    }
    return "incorrect <br/>";

}

function getCategoryResultsHTML(category, categoryName){
    CategoryResultHTML = '';
    for (i = 0; i < category.length; i++){
        currentCategory = category[i];
        CategoryResultHTML += getQuestionResultHTML(category[i]);
    }
    return CategoryResultHTML;
}

function generateResults(){
    var resultsDiv = document.getElementById('results');
    for (const property in questionsContent) {
        resultsDiv.innerHTML += getCategoryResultsHTML(questionsContent[property], property);
    }
}